var model = {
    currentCat: null,
    cats: [
        {
            name: "Cat 1",
            path: "img/Cat.jpg",
            count: 0
        },
        {
            name: "Cat 2",
            path: "img/Cat2.jpg",
            count: 0
        },
        {
            name: "Cat 3",
            path: "img/Cat3.jpg",
            count: 0
        },
        {
            name: "Cat 4",
            path: "img/Cat4.jpg",
            count: 0
        },
        {
            name: "Cat 5",
            path: "img/Cat5.jpg",
            count: 0
        },
    ],
    adminVisible: false
};

var controller = {
    init: function() {
        model.currentCat = model.cats[0];

        catListView.init();
        catView.init();
        adminView.init();
    },

    getCurrentCat: function() {
        return model.currentCat;
    },

    getAllCats: function() {
        return model.cats;
    },

    setCurrentCat: function(cat) {
        model.currentCat = cat;
    },

    incrementCounter: function() {
        model.currentCat.count++;
        catView.render();
    },

    toggleAdmin: function() {
        if (!model.adminVisible) {
            model.adminVisible = true;
            adminView.show();
        }
        else {
            model.adminVisible = false;
            adminView.hide();
        }
    },
};

var catView = {
    init: function() {
        this.catElem = document.getElementById("cat");
        this.catNameElem = document.getElementById("cat-name");
        this.catImgElem = document.getElementById("cat-img");
        this.catCountElem = document.getElementById("cat-count");
        this.adminToggle = document.getElementById("admin-toggle");
        this.adminArea = document.getElementById("admin-area");

        this.catImgElem.addEventListener("click", function() {
            controller.incrementCounter();
            adminView.render();
        });

        this.render();
    },

    render: function() {
        var currentCat = controller.getCurrentCat();
        this.catNameElem.textContent = currentCat.name;
        this.catImgElem.src = currentCat.path;
        this.catCountElem.textContent = currentCat.count;
    }
};

var catListView = {
    init: function() {
        this.catListElem = document.getElementById("cat-list");
        this.render();
    },

    render: function() {
        var cat, elem, i;
        var cats = controller.getAllCats();
        this.catListElem.innerHTML = "";
        for (var i = 0; i < cats.length; i++) {
            cat = cats[i];

            elem = document.createElement("li");
            elem.textContent = cat.name;

            elem.addEventListener("click", (function(catCopy) {
                return function() {
                    controller.setCurrentCat(catCopy);
                    catView.render();
                    adminView.render();
                };
            })(cat));

            this.catListElem.appendChild(elem);
        }
    }
};

var adminView = {
    init: function() {
        this.adminToggle = document.getElementById("admin-toggle");
        this.adminArea = document.getElementById("admin-area");
        this.adminName = document.getElementById("admin-name");
        this.adminPath = document.getElementById("admin-path");
        this.adminCount = document.getElementById("admin-count");
        this.adminSave = document.getElementById("admin-save");
        this.adminCancel = document.getElementById("admin-cancel");

        this.adminToggle.addEventListener("click", function() {
            controller.toggleAdmin();
        });

        this.adminCancel.addEventListener("click", function() {
            controller.toggleAdmin();
        });

        this.adminSave.addEventListener("click", function() {
            adminView.save();
        });

        this.render();
    },

    show: function() {
        this.adminArea.style.display = "initial";
    },

    hide: function() {
        this.adminArea.style.display = "none";
    },

    save: function() {
        var currentCat = controller.getCurrentCat();
        currentCat.name = this.adminName.value;
        currentCat.path = this.adminPath.value;
        currentCat.count = this.adminCount.value;
        catListView.render();
        catView.render();
        this.render();
    },

    render: function() {
        var currentCat = controller.getCurrentCat();
        this.adminName.value = currentCat.name;
        this.adminPath.value = currentCat.path;
        this.adminCount.value = currentCat.count;
    }
};

controller.init();
