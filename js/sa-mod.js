var model = {
    students: [
        {
            name: "Slappy the Frog",
            present: []
        },
        {
            name: "Lilly the Lizard",
            present: []
        },
        {
            name: "Paulrus the Walrus",
            present: []
        },
        {
            name: "Gregory the Goat",
            present: []
        },
        {
            name: "Adam the Anaconda",
            present: []
        }
    ]
};

var controller = {
    init: function () {
        view.init();
        view.check();
        view.markMissed();
    },

    getStudent: function (name) {
        for (var i = 0; i < model.students.length; i++) {
            if (model.students[i].name === name)
                return model.students[i];
        }
    },

    insertRecord: function (name, id) {
        this.getStudent(name).present.push(id);
    },

    removeRecord: function (name, id) {
        if (this.getStudent(name).present.includes(id)) {
            var index = this.getStudent(name).present.indexOf(id);
            this.getStudent(name).present.splice(index, 1);
        }
    },

    getModel: function () {
        if (localStorage.getItem('model'))
            model = JSON.parse(localStorage.getItem('model'));
        return model;
    },

    saveModel: function () {
        localStorage.setItem('model', JSON.stringify(model));
    }
};

var view = {
    init: function () {
        var allChckBxs = document.getElementsByTagName("input");

        for (var i = 0; i < allChckBxs.length; i++) {
            allChckBxs[i].id = i;
            allChckBxs[i].addEventListener("click", function () {
                var name = this.parentElement.parentElement.children[0].innerHTML;
                var missed = this.parentElement.parentElement.children[13];
                if (this.checked)
                    controller.insertRecord(name, this.id);
                else if (!this.checked)
                    controller.removeRecord(name, this.id);
                view.render(name, missed);
                controller.saveModel();
            });
        }
    },

    render: function (name, missed) {
        missed.innerHTML = 12 - controller.getStudent(name).present.length;
    },

    check: function () {
        var student = controller.getModel().students;
        for (var i = 0; i < student.length; i++) {
            for (var j = 0; j < student[i].present.length; j++) {
                document.getElementById(student[i].present[j]).checked = true;
            }
        }
    },

    markMissed: function () {
        var misses = document.getElementsByClassName("missed-col");
        for (var i = 1; i < misses.length; i++) {
            var name = misses[i].parentElement.children[0].innerHTML;
            misses[i].innerHTML = 12 - controller.getStudent(name).present.length;
        }
    }
};

controller.init();