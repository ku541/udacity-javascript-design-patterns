var cats = [
        {
            clickCount: 0,
            nickname: ['Nickname 1', 'Nickname 2', 'Nickname 3'],
            name: "Cat 1",
            imgSrc: "/home/kusal/Projects/JDP/img/Cat.jpg",
        },
        {
            clickCount: 0,
            nickname: ['Nickname 4', 'Nickname 5', 'Nickname 6'],
            name: "Cat 2",
            imgSrc: "/home/kusal/Projects/JDP/img/Cat.jpg",
        },
        {
            clickCount: 0,
            nickname: ['Nickname 7', 'Nickname 8', 'Nickname 9'],
            name: "Cat 3",
            imgSrc: "/home/kusal/Projects/JDP/img/Cat3.jpg",
        },
        {
            clickCount: 0,
            nickname: ['Nickname 10', 'Nickname 11', 'Nickname 12'],
            name: "Cat 4",
            imgSrc: "/home/kusal/Projects/JDP/img/Cat4.jpg",
        },
        {
            clickCount: 0,
            nickname: ['Nickname 13', 'Nickname 14', 'Nickname 15'],
            name: "Cat 5",
            imgSrc: "/home/kusal/Projects/JDP/img/Cat5.jpg",
        },
    ],

Cat = function (data) {
    this.clickCount = ko.observable(data.clickCount);
    this.name = ko.observable(data.name);
    this.nickname = ko.observable(data.nickname);
    this.imgSrc = ko.observable(data.imgSrc);

    this.age = ko.computed(function () {
        var clicks = this.clickCount();
        if (clicks === 0) {
            return "Infant";
        } else if (clicks > 0 && clicks < 4) {
            return "Toddler";
        } else if (clicks > 3 && clicks < 6) {
            return "Preschooler";
        } else if (clicks > 5 && clicks < 13) {
            return "Gradeschooler";
        } else if (clicks > 12 && clicks < 20) {
            return "Teen";
        } else if (clicks > 19 && clicks < 22) {
            return "Young Adult";
        } else {
            return "Adult";
        }
    }, this);
};

var ViewModel = function () {
    var self = this;

    this.catList = ko.observableArray();

    cats.forEach(function (cat) {
        self.catList.push(new Cat(cat));
    });

    this.currentCat = ko.observable(this.catList()[0]);

    this.incrementCounter = function () {
        this.clickCount(this.clickCount() + 1);
    };

    this.setCat = function (cat) {
        self.currentCat(cat);
    };
};

ko.applyBindings(new ViewModel());
